# README

**Config files:**

* [config.json](./#configjson)
* [adunits.json](./#adunitsjson)
* [deviceDetection.json](./#devicedetectionjson)
* [modules.json](./#modulesjson)

[**Custom script files**](./#custom-script-files)

[**Key-Values**](./#wrapper-key-values)

[**RichMediaFormats Modules**](./#richmediaformats-modules)

[**RichMediaFormats Core Modules**](./#richmediaformats-core-modules)

[**Realease Notes**](./#release-notes)

### config.json

This file contains basic configuration regarding the wrapper global settings.

#### `id`

An identification number that can link the wrapper to a site on other platforms.

Default: `9999`

```javascript
{
	"id" : 123
}
```

#### `code`

The identification name of the wrapper.

:exclamation: It must be unique.

```javascript
{
	"code" : "abc"
}
```

#### `file`

The name of the file as hosted on the CDN. This is used to build, deploy, and purge.

:exclamation: It must be unique.

```javascript
{
	"file" : "prefix.code.js"
}
```

#### `blockAutoBuild`

If it is `true` it will not allow it to be automatically built.

Default: `false`

```javascript
{
	"blockAutoBuild" : true
}
```

#### `globalVar`

The variable that prebid will use. The wrapper uses a custom variable name for prebid to not have any conflicts with other wrappers on the page.

```javascript
{
	"globalVar" : "pbjs"
}
```

#### `targetingPrefix`

Set the prefix of targeting key values used for sending bids to GAM. For example `[prefix]_bidder`, `[prefix]_hb` , `[prefix]_adid`.

Note: It will disable [Prebid Server](./#wrapperprebidserverenabled), [Safe Frames Under Floor](./#dfpforcesafeframesunderfloor), [Safe Frames for Adunits](./#dfpforcesafeframesforadunits), [Safe Frames](./#dfpforcesafeframes)

Default: `hb`

```javascript
{
	"targetingPrefix" : "chp"
}
```

#### `url`

The URL to the site where the wrapper will be implemented.

```javascript
{
	"url" : "https://example.com"
}
```

#### `dfpAccount`

The GAM account ID of the main account.

```javascript
{
	"dfpAccount" : "123456789"
}
```

#### `dfpChildAccount`

The GAM account ID of the child account. If this is set for an MCM network, it will use the GAM tags appropriate for MCM, using both the main and child account. Do not set it if it is not an MCM network.

```javascript
{
	"dfpChildAccount" : "4567890123"
}
```

#### `adunitMapping`

The method of mapping the div tags implemented in the page with the config built in the wrapper. There are two possible values:

1. `adunit` - it will match it based on adunit (`data-aa-adunit` of the div -> with the property `adunit` from adunits.json)
2. `targeting` = it will match it based on a key targeting (`data-aa-targeting` of the div -> with the property `targeting` from adunits.json)

Default: `adunit`

```javascript
{
	"adunitMapping" : "adunit"
}
```

#### `adunitMappingKey`

The name of the keyvalue that will be used for mapping. It only works when `adunitMapping` is `targeting`.

Default: `pos`

```javascript
{
	"adunitMappingKey" : "location"
}
```

#### `gist`

The URL to the location of tags that the publisher needs to implement in the page

```javascript
{
	"url" : "https://gist.github.com/abc/123"
}
```

#### `analytics.clarity`

To enable Clarity analytics, use the ID provided in the dashboard

Default: `false`

```javascript
{
	"analytics" : {
		"clarity" : "4l37sx2uso"
	}
}
```

#### `analytics.gtm`

Enable a GTM container.

It takes two parameters:

1. `containerId` : the Google Tag Manager Container ID. It is required, without this the GTM container will not be loaded
2. `dataLayer` : the name of the variable for Data Layer. Default is `dataLayer` and if not set will use default.

```javascript
{
	"analytics" : {
		"gtm" : {
			"containerId" : "GTM-ABCDE123",
			"dataLayer" : "customDataLayerVariable"
		}
	}
}
```

#### `analytics.prebid`

To enable a prebid analytics provider (can take an Array for multiple ones, or Object for one)

```javascript
{
	"analytics" : {
		"prebid" : [
                       {
                        "provider": "provider1Analytics",
                            "options": {
                                        "partnerId": 123
                            }
                       },
                       {
                        "provider": "provider2Analytics",
                            "options": {
                                        "partnerId": 123
                            }
                       } 
                   ]
	}
}
```

#### `wrapper.timeout`

The maximum time in ms that the header bidding auction will wait for bids

Default: `2000`

```javascript
{
	"wrapper" : {
		"timeout" : 2000
	}
}
```

#### `wrapper.conditionalTimeout`

The maximum time in ms that the header bidding auction will wait for bids for each device type (as defined in [deviceDetection.json](./#devicedetectionjson) file). It will overwrite the default `timeout` if the device is detected.

```javascript
{
	"wrapper" : {
		"conditionalTimeout" : {
            "desktop": 2000,
            "tablet" : 1500,
            "mobile" : 1000
        },
	}
}
```

#### `wrapper.timeoutFallback`

The maximum time in ms that the wrapper will wait until it will make the GAM ad request

Default: `5000`

```javascript
{
	"wrapper" : {
		"timeoutFallback" : 5000
	}
}
```

#### `wrapper.bidRequestWaitTime`

The time in ms that the processing of ads will be throttled, to make all processing at the same time

Default: `50`

```javascript
{
	"wrapper" : {
		"bidRequestWaitTime" : 50
	}
}
```

#### `wrapper.disableBiddersToStorage`

If is enabled, it will store all bidders that throw an error and will exclude those bidders from client side auctions in the following auctions. The value is numeric and represents the ttl of the value in storage for each bidder in seconds.

```javascript
{
	"wrapper" : {
		"disableBiddersToStorage" : 90000
	}
}
```

#### `wrapper.disableBidders`

Array of bidder names that will be excluded from all auctions. (are completely disabled)

```javascript
{
	"wrapper" : {
		"disableBidders" : ["appnexus","pubmatic"]
	}
}
```

#### `wrapper.disableClientSideAdapters`

Array of bidder names that will be excluded from all client side auctions. (they will still run in S2S)

```javascript
{
	"wrapper" : {
		"disableClientSideAdapters" : ["appnexus","pubmatic"]
	}
}
```

#### `wrapper.geoFilter`

Filter out bidders based on Geo Detection (Geo Detection is async and might be resolved after the auction is initiated, so this functionality is not 100% guaranteed). It takes an array of objects. Each object key is a for a bidder.

For each key (bidder) there are two filter options:

* whitelist: It will enable the bidder only for countries specified
* blacklist: It will disable the bidder for countries specified

Each of the option takes an array of country codes (ISO).

```javascript
{
	"wrapper" : {
		"geoFilter" : {
			"33across" : {
				"whitelist" : ["US"],
				"blacklist" : []
			}
		}
	}
}
```

#### `wrapper.geoFilterUserId`

Filter out User IDs based on Geo Detection (Geo Detection is async and might be resolved after the User ID is initiated, so this functionality is not 100% guaranteed). It takes an array of objects. Each object key is a for a User ID.

For each key (User ID) there are two filter options:

* whitelist: It will enable the User ID only for countries specified
* blacklist: It will disable the User ID for countries specified

Each of the option takes an array of country codes (ISO).

```javascript
{
	"wrapper" : {
		"geoFilterUserId" : {
			"identityLink" : {
				"whitelist" : ["US","CA","AR","BR","MX","BE","FR","DE","IT"],
				"blacklist" : []
			}
		}
	}
}
```

#### `wrapper.disableInitialProcessAdsOnPage`

The wrapper is automatically processing ads on pageload, If this is `true` it will disable any automatic processing of ads.

Default: `false`

```javascript
{
	"wrapper" : {
		"disableInitialProcessAdsOnPage" : true
	}
}
```

#### `wrapper.prebidServer.enabled`

Enable Prebid Server to use in Hybrid bidding (both Client and S2S)

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true
		}
	}
}
```

#### `wrapper.prebidServer.accountId`

Prebid Server Account ID. If the Prebid Server is requiring an Account ID.

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true,
			"accountId": "123"
		}
	}
}
```

#### `wrapper.prebidServer.endpoint`

Update Prebid Server Endpoint. The URL endpoint for auction.

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true,
			"accountId": "123",			
            "endpoint": "https://prebid.mydomain.com/pbs/v1/openrtb2/auction"
		}
	}
}
```

#### `wrapper.prebidServer.syncEndpoint`

Update Prebid Server Sync Endpoint. The URL endpoint for user sync.

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true,
			"accountId": "123",			
            "endpoint": "https://prebid.mydomain.com/pbs/v1/openrtb2/auction",
            "syncEndpoint": "https://prebid.mydomain.com/pbs/v1/cookie_sync"
		}
	}
}
```

#### `wrapper.prebidServer.maxTimeout`

Update Prebid Server Max Timeout. Maxium time allowed for the S2S auction to respond

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true,
			"accountId": "123",			
            "endpoint": "https://prebid.mydomain.com/pbs/v1/openrtb2/auction",
            "syncEndpoint": "https://prebid.mydomain.com/pbs/v1/cookie_sync",
            "maxTimeout": 1700
		}
	}
}
```

#### `wrapper.prebidServer.bidders`

Update Prebid Server Bidders. The list of bidders that are allowed to run on S2S

```javascript
{
	"wrapper" : {
		"prebidServer" : {
			"enabled" : true,
			"accountId": "123",			
            "endpoint": "https://prebid.mydomain.com/pbs/v1/openrtb2/auction",
            "syncEndpoint": "https://prebid.mydomain.com/pbs/v1/cookie_sync",
            "maxTimeout": 1700,
            "bidders": ["appnexus","sovrn","pubmatic","triplelift","openx"]
		}
	}
}
```

#### `bidAdjustments.pubshare`

All header bidding bids will be adjusted with this value.

Default: `1`

```javascript
{
	"bidAdjustments" : {
		"pubshare" : 0.8
	}
}
```

#### `bidAdjustments.s2s`

Header bidding bids received through Server 2 Server will be adjusted to this value

Default: `1`

```javascript
{
	"bidAdjustments" : {
		"s2s" : 0.95
	}
}
```

#### `bidAdjustments.networks`

Adjusting bid requests for individual networks.

```javascript
{
	"bidAdjustments" : {
		"networks" : {
			"pubmatic" : 0.97,
			"districtm" : 0.9
		}
	}
}
```

#### `lazyLoading.preViewLenght`

The distance of the ads in page, in pixels, from the active portview when ads will be requested

Default: `1000`

```javascript
{
	"lazyLoading" : {
		"preViewLenght" : 1500
	}
}
```

#### `lazyLoading.preViewPorts`

The distance of the ads in page, in number of viewports, depending on the device, when ads will be requested. If set, this will overwrite `lazyLoading.preViewLenght`

Default: \[notset]

```javascript
{
	"lazyLoading" : {
		"preViewPorts" : 2
	}
}
```

#### `lazyLoading.scrollEventDelay`

The time that the scroll event is throttled to mitigate latency.

Default: `400`

```javascript
{
	"lazyLoading" : {
		"scrollEventDelay" : 400
	}
}
```

#### `targeting.refreshIteration`

The name of the keyvalue used to count refreshes

Default: `refresh_count`

```javascript
{
	"targeting" : {
		"refreshIteration" : "refresh_count"
	}
}
```

#### `targeting.refreshedInventory`

The name of the keyvalue used to mark if the inventory is refreshed or not

Default: `false`

```javascript
{
	"targeting" : {
		"refreshedInventory" : "refreshed"
	}
}
```

#### `targeting.refreshedAccelerateSeconds`

If enabled it will set a key value representing the seconds that the refreshed was accelerated with

Default: `false`

```javascript
{
	"targeting" : {
		"refreshedAccelerateSeconds" : true
	}
}
```

#### `targeting.speed`

If `true` it sends keyvalues relative to speed. The key values are:

* wrap\_l: the load time of the wrapper (relative to page navigation)
* gpt\_l : the load time of googletag library (relative to wrapper load time)
* page\_r : the load time of the page (relative to wrapper load time)
* waai : the time until the auction initiated (relative to wrapper load time), tracked only for first auction
* waae : the time until the auction ended (relative to auction init)
* wabt : the bidders that timeout (relative to what prebid detects as timeout)
* wabtr : the bidders that timeout by not sending a bid until the GAM ad request is made
* cndl : CoNnection DownLink, 0 unknown, 0.1 under 1MB, 1 for 1-9.99 MB, 10 for 10-99.99, 100 for 100 - 999.99, 1000 for >= 1000
* cnrtt : CoNnection RTT, 0 unknown, 10 <=10, 100 <= 100,... ,900 <= 900, 1000 >= 1000
* cntp : CoNnection TyPe, na, bluetooth, cellular, ethernet, none, wifi, wimax, other, unknown
* cnet : CoNnection Effective Type, na, 'slow-2g', '2g', '3g', '4g', '5g'
* cnsd : CoNnection Save Data, true or false

Default: `true`

#### `targeting.pageAdDensity`

If `true` it sends keyvalues relative to Page Ad Density. The key values are:

* padpr (Page Ad Density PreRequest): represents ad densitity before requests based on biggest ad size available
* pad (Page Ad Density) : represents ad density after ads had been rendered

Default: `true`

```javascript
{
	"targeting" : {
		"pageAdDensity" : true
	}
}
```

#### `targeting.utmKeyValues`

If `true` it sends keyvalues relative to UTM values. The key values are:

* utm\_source
* utm\_medium
* utm\_campaign
* utm\_content

Default: `true`

```javascript
{
	"targeting" : {
		"utmKeyValues" : true
	}
}
```

#### `targeting.uids`

If `true` it sends keyvalues relative to User Identification Services/Providers. The key values are:

* uids: It will send the value of each User Id Service/Provider that identify an user
* uids\_c : It will send the count of all User Id Services that identified an user
* individual key value for each User Id Service/Provider: It will set a key with the name of the User Id Service/Provider

Default: `false`

```javascript
{
	"targeting" : {
		"uids" : true
	}
}
```

#### `targeting.topicsapi`

If `true` it sends keyvalues with the values from Topics API interests [Google Privacy Sandbox Topics](https://developers.google.com/privacy-sandbox/relevance/topics)

Default: `false`

```javascript
{
	"targeting" : {
		"topicsapi" : true
	}
}
```

#### `targeting.gads`

If `true` it sends keyvalues relative to the GAM cookie gads is set or not

Default: `false`

```javascript
{
	"targeting" : {
		"gads" : true
	}
}
```

#### `targeting.cookieEnabled`

If `true` it sends keyvalues relative if browser allows cookies from 3rd party scope [Mozilla documentation cookieEnabled](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/cookieEnabled)

Default: `false`

```javascript
{
	"targeting" : {
		"cookieEnabled" : true
	}
}
```

#### `targeting.browserLangCode`

If `true` it sends keyvalues relative to the user's language as setup in the browser

Default: `false`

```javascript
{
	"targeting" : {
		"browserLangCode" : true
	}
}
```

#### `targeting.referrer`

If `true` it sends keyvalues relative to the referrer domain

Default: `false`

```javascript
{
	"targeting" : {
		"referrer" : true
	}
}
```

#### `targeting.domain`

If `true` it sends keyvalues relative to the domain where the wrapper is loaded

Default: `false`

```javascript
{
	"targeting" : {
		"domain" : true
	}
}
```

#### `targeting.code`

If `true` it sends keyvalue `wrapCode` relative to the wrapper code

Default: `false`

```javascript
{
	"targeting" : {
		"code" : true
	}
}
```

#### `targeting.id`

If `true` it sends keyvalue `wrapId` relative to the wrapper code

Default: `false`

```javascript
{
	"targeting" : {
		"id" : true
	}
}
```

#### `targeting.device`

If `true` it sends keyvalues relative to the device detected from the browser user agent

Default: `false`

```javascript
{
	"targeting" : {
		"device" : true
	}
}
```

#### `targeting.browser`

If `true` it sends keyvalues relative to the browser detected from the browser user agent

Default: `false`

```javascript
{
	"targeting" : {
		"browser" : true
	}
}
```

#### `targeting.os`

If `true` it sends keyvalues relative to the os detected from the browser user agent

Default: `false`

```javascript
{
	"targeting" : {
		"os" : true
	}
}
```

#### `abtests.uids`

It will enable the A/B test for Uids (when an User Identification Service/Provider identified an user it will split the traffic into 50/50 and half of time it will send UserId to Networks and half not). The key values set to GAM it will be 'nameOfUserID+`test`, with values `true` - UserID sent to network, `false` - UserID not set to network, `na` - UserID not identified

Default: `false`

```javascript
{
	"abtests" : {
		"uids" : ["idl_env","lipb","lotamePanoramaId","id5id","33acrossId","teadsId","fabrickId"]
	}
}
```

#### `refresh.stopAdvertisers`

Array of GAM advertiser/company IDs that will stop the refresh after an impression from them is displayed (is numeric, not string)

```javascript
{
	"refresh" : {
		"stopAdvertisers" : []
	}
}
```

#### `refresh.pauseOutOfFocus`

When `true` it will not refresh until the page is focus

Default: `false`

```javascript
{
	"refresh" : {
		"pauseOutOfFocus" : false
	}
}
```

#### `refresh.pauseOutOfView`

Takes two properties:

* `threshold` - Float from `0` to `1` it will refresh only when ad is in active viewport. The value represents the threshold; `0` representing one pixel, `1` all pixels of the element.
* `adunits` - array of strings representing adunits that pauseOutOfView will apply to. It takes also the wildcard `*` for all adunits

```javascript
{
	"refresh" : {
		"pauseOutOfView" : {
			"threshold" : 1,
			"adunits" : ["*"]
		}
	}
}
```

```javascript
{
	"refresh" : {
		"pauseOutOfView" : {
			"threshold" : 0,
			"adunits" : ["/123/AdUnit1","/123/AdUnit2"]
		}
	}
}
```

#### `refresh.maxLimit`

The maximum number of refreshes

Default: `false`

```javascript
{
	"refresh" : {
		"maxLimit" : 100
	}
}
```

#### `refresh.maxEmpty`

The maximum number of refreshes with an empty response from GAM

Default: `false`

```javascript
{
	"refresh" : {
		"maxEmpty" : 100
	}
}
```

#### `refresh.maxConsecutiveEmpty`

The maximum number of consecutive refreshes with an empty response from GAM

Default: `false`

```javascript
{
	"refresh" : {
		"maxConsecutiveEmpty" : 100
	}
}
```

#### `refresh.forceRefreshRateForAdvertiser`

Change the refresh rate for specific GAM advertiser/company

Default: `false`

```javascript
{
	"refresh" : {
		"forceRefreshRateForAdvertiser" : [
			{"advertiserId": 4721606053, "refreshRate":30},
			{"advertiserId": 4607025053, "refreshRate":30},
			{"advertiserId": 4607025017, "refreshRate":30},
			{"advertiserId": 4805864332, "refreshRate":30},
			{"advertiserId": 4805864332, "refreshRate":30}
		]
	}
}
```

#### `refresh.accelerate`

It will refresh earlier with a specific amount of seconds if the estimated CMP is higher than the value specified.

Default: `false`

```javascript
{
	"refresh" : {
		"accelerate" : [
		      {"estCPM": 1, "seconds": 3},
		      {"estCPM": 1.5, "seconds": 5},
		      {"estCPM": 2, "seconds": 10},
		      {"estCPM": 2.5, "seconds": 15}
		]
	}
}
```

#### `refresh.continueOnEmpty`

Only applies to [viewable refresh](./#refreshviewable).

If GAM returns an empty response, there is no ad to be viewable, so viewable refresh will stop.

If this is enabled, if viewable refresh is enabled and there is an empty response from GAM, refresh will not stop, one time refresh is scheduled with the same refresh rate as the viewable one. Practically it transforms the next viewable refresh into timed refresh.

[refresh.maxLimit](./#refreshmaxlimit) , [refresh.maxEmpty](./#refreshmaxempty) , [refresh.maxConsecutiveEmpty](./#refreshmaxconsecutiveempty) applies. Refreshes will be counted and refresh will stop if they reached one of the limits.

[refresh.accelerate](./#refreshaccelerate) applies. The refresh is accelerated if there is a bid according to settings.

[refresh.pauseOutOfFocus](./#refreshpauseoutoffocus) applies and it enabled it will not refresh while tab is out of focus.

[refresh.stopAdvertisers](./#refreshstopadvertisers) , [refresh.forceRefreshRateForAdvertiser](./#refreshforcerefreshrateforadvertiser) does not apply. If the response is empty there is no advertiser.

Default: `false`

```javascript
{
	"refresh" : {
		"continueOnEmpty" : true
	}
}
```

#### `deviceDetection`

The method of detecting the size of the site. There are two possible values:

1. `window` - it will detect the size of the window/tab (if the window is resized or the tag is inside an iframe and cannot access the top window, it will detect that size)
2. `screen` = it will detect the screen size, not taking in account the actual window/tab

Default: `window`

```javascript
{
	"deviceDetection" : "window"
}
```

#### `dfp.collapseEmptyDivs`

If GAM has an empty response, collapse the size of the divs

Default: `false`

```javascript
{
	"dfp" : {
		"collapseEmptyDivs" : false
	}
}
```

#### `dfp.emtpyCreativeFallback`

If GAM has an empty response, but there is a header bidding bid, deliver the ad without GAM.

Default: `true`

```javascript
{
	"dfp" : {
		"emtpyCreativeFallback" : true
	}
}
```

#### `dfp.failedToFetchFallback`

After a specific amount a time, the adunits are checked if GAM displayed an ad, if not and there are HB bids available, deliver the ad without GAM.

Default: `false`

```javascript
{
	"dfp" : {
		"failedToFetchFallback" : 3000
	}
}
```

#### `dfp.houseFallback`

Specify creatives to serve in case there is no demand from GAM or Prebid. Need to have [dfp.emtpyCreativeFallback](./#dfpemtpycreativefallback) or [dfp.failedToFetchFallback](./#dfpfailedtofetchfallback) enabled to work.

The value is an object where the keys are sizes and values are the HTML creative. It will deliver the creative corresponding to the size of the adunit. If the adunit has multiple sizes and multiple creatives are available, it will be randomly selected.

Default: `false`

```javascript
{
	"dfp" : {
		"emtpyCreativeFallback": true,
        "failedToFetchFallback": 3000,
		"houseFallback":{
			"300x250" : "<!-- this is a house 300x250 -->",
			"336x280" : "<!-- this is a house 336x280 -->",
			"728x90" : "<!-- this is a house 728x90 -->",
			"468x60" : "<!-- this is a house 468x60 -->",
			"970x90" : "<!-- this is a house 970x90 -->",
			"970x250" : "<!-- this is a house 970x250 -->",
			"300x600" : "<!-- this is a house 300x600 -->",
			"320x50" : "<!-- this is a house 320x50 -->",
			"320x100" : "<!-- this is a house 320x100 -->"
		}
	}
}
```

#### `dfp.forceSafeFramesUnderFloor`

It will enable GAM's SafeFrames if the Header Bidding bid is under the floor.

Note: If this is enabled, it will disable [Amazon](./#amazon) and bidders that are not OK with Safe Frames

Default: `false`

```javascript
{
	"dfp" : {
		"forceSafeFramesUnderFloor" : 2.1
	}
}
```

#### `dfp.forceSafeFramesForAdunits`

It will enable GAM's SafeFrames on specific adunits. Need to specify the full adunit path.

Note: If this is enabled, it will disable [Amazon](./#amazon) for the adunits that is enabled for. If this is enabled it will disable bidders that are not ok with Safe Frames.

Default: `false`

```javascript
{
	"dfp" : {
		"forceSafeFramesForAdunits" : ["/123/AdUnit1","/123/AdUnit2"]
	}
}
```

#### `dfp.forceSafeFrames`

If `true` it will enable GAM's SafeFrames.

Note: If this is enabled, it will disable [Amazon](./#amazon) and bidders that are not OK with Safe Frames

Default: `false`

```javascript
{
	"dfp" : {
		"forceSafeFrames" : true
	}
}
```

#### `dfp.adExpansion`

Configuring GAM's Ad Expansions.

* [Expand ads on desktop and tablet](https://support.google.com/admanager/answer/9384852)
* [Expand ads on mobile web (partial screen)](https://support.google.com/admanager/answer/9117822)

Options:

* If `true` it will enable GAM's Ad Expansions. The setting will overwrite the setting in GAM's UI.
* If `false` it will disable GAM's Ad Expansion. The setting will overwrite the setting in GAM's UI.
* Not set. Will use the setting in GAM's UI

Default: not set

```javascript
{
	"dfp" : {
		"adExpansion" : true
	}
}
```

#### `prebid`

Prebid configuration as described here [Prebid documentation setConfig](rebid.org/dev-docs/publisher-api-reference/setConfig.htmlhttps:/docs.prebid.org/dev-docs/publisher-api-reference/setConfig.html)

```javascript
{
	"prebid" : {
	    "priceGranularity" : {
	        "buckets": [
	                    {
                            "precision": 2, 
                            "max": 20,
                            "increment": 0.01
	                    }
	                ]
	    },
	    "schain": {
	    	"validation": "strict",
	    	"config": {
				"ver":"1.0",
				"complete": 1,
				"nodes": [{ "asi":"network.com", "sid":"abc123", "hp":1 }]
			}
		}
	}
}
```

#### `prebid.bidCaching`

It will store bids to be reused in future auctions. If `false` is disabled. `maxLength` - the maximum size of the bid in bytes, to not allow bids with very big creatives `floor` - store only bids above this CPM

Default: `false`

```javascript
{
	"prebid" : {
		"bidCaching" : {
	    	"maxLength": 100000, 
	    	"floor": 0.10
	    }
	}
}
```

#### `prebidBidderSpecific`

Prebid configuration for sepecific bidders [Prebid documentation setBidderConfig](https://docs.prebid.org/dev-docs/publisher-api-reference/setBidderConfig.html). Can take an Array for multiple ones, or Object for a single one.

```javascript
{
    "prebidBidderSpecific" : [
    	{
        	"bidders" : ["rubicon","pubmatic"],
        	"config" : {    		
    		    "schain": {
    		    	"validation": "strict",
    		    	"config": {
    					"ver":"1.0",
    					"complete": 1,
    					"nodes": [{ "asi":"network.com", "sid":"abc123", "hp":1 }]
    				}
    			}
        	}
        },
    	{
        	"bidders" : ["gourmetads"],
        	"config" : {    		
    		    "schain": {
    		    	"validation": "strict",
    		    	"config": {
    					"ver":"1.0",
    					"complete": 1,
    					"nodes": [{ "asi":"othernetwork.com", "sid":"cdf345", "hp":1 }]
    				}
    			}
        	}
        }
    ]
}
```

#### `amazon`

It will use the amazon/a9 wrapper

* `enabled` - it will enable/disable the wrapper
* `pubID` - the amazon ID, from the AM or Dashboard
* `adServer` - the adserver

```javascript
{
	"amazon" : {
    	"enabled" : true,
	    "pubID": "123",
	    "adServer": "googletag"
	}
}
```

#### `amazon.declareRefresh`

Optional parameter for Amazon/A9 Config that will send adRefresh parameter as configuration, according to [Amazon/A9 Config documentation](https://ams.amazon.com/webpublisher/uam/docs/reference/api-reference.html#adrefresh)

* `true` - it will sent adRefresh to `0` when initial requests and `1` when refresh requests
* `false` - it will sent adRefresh at all

Default: \[not set]

```javascript
{
	"amazon" : {
    	"enabled" : true,
	    "pubID": "123",
	    "adServer": "googletag",
	    "declareRefresh" : true
	}
}
```

#### `forceReWrittingPBJS`

If `true` it clones the prebid global variable as pbjs to not have any conflicts with the GAM creatives. Where there are multiple instances of prebid on the page, this must be disabled.

Default: `true`

```javascript
{
	"forceReWrittingPBJS" : true
}
```

### adunits.json

This file contains settings individually for each adunit of the wrapper.

Each entry is an adunit.

Cannot have multiple adunits mapped to the same div.

You can have multiple divs mapped to the same adunit.

Each adunit has the following properties:

#### `adunit`

The GAM adunit path

```javascript
{
	"adunit" : "/123/abc"
}
```

#### `device`

An array of devices (as defined in [deviceDetection.json](./#devicedetectionjson) file) where the adunit will display.

```javascript
{
	 "device": ["bigDesktop","desktop","tablet"]
}
```

#### `mediaTypes`

MediaTypes in the format accepted by prebid ([More info](https://docs.prebid.org/dev-docs/adunit-reference.html#adunitmediatypes))

```javascript
{
	"mediaTypes" : {"banner" : {"sizes" : [[728,90],[970,90],[970,250],[728,250]] }}
}
```

#### `conditionalMediaTypes`

MediaTypes for each device type (as defined in [deviceDetection.json](./#devicedetectionjson) file). It will overwrite the default MediaTypes if the device is detected.

```javascript
{
	"conditionalMediaTypes" : {
        "bigDesktop":       {"banner" : {"sizes" : [[728,90],[970,90],[970,250],[728,250],[1200,280]] }},
        "desktop" :         {"banner" : {"sizes" : [[728,90],[970,90],[970,250],[728,250]] }},
        "tablet" :          {"banner" : {"sizes" : [[728,90],[728,250]] }},
        "mobile" :          {"banner" : {"sizes" : [[1,1]] }}
    }
}
```

#### `additionalNonHBSizes`

Sizes that are added to the GAM request, but not the header bidding requests.

```javascript
{
	"additionalNonHBSizes" : ["fluid"]
}
```

#### `conditionalNonHBSizes`

Additional sizes for each device type (as defined in [deviceDetection.json](./#devicedetectionjson) file). It will overwrite the default additionalNonHBSizes if the device is detected.

```javascript
{
	"conditionalNonHBSizes" : {
	        "bigDesktop":       ["fluid"],
	        "desktop" :         ["fluid"],
	        "tablet" :          ["fluid"],
	        "mobile" :          [[1,1]]
	    }
}
```

#### `outofpage`

If `true` defines the adunit as Out Of Page.

Default: `false`

```javascript
{
	"outofpage" : "true"
}
```

#### `targeting`

Keyvalue name combination to send as targeting to GAM. Takes an object where the key of the object is the name of the KV. The values can be a string or an array of strings.

```javascript
{
	"targeting" : 	{
			"pos" : "top",
			"age" : "13",
			"gender" : "m"
    }
}
```

#### `lazyLoaded`

If `true` enables lazy loading for the adunit

Default: `false`

```javascript
{
	"lazyLoaded" : "true"
}
```

#### `richMediaFormat`

Make the adunit a richmedia format.

```javascript
{
	"richMediaFormat": {"type" : "adhesion" , "options" : {"closeButton":"true"}}
}
```

#### `refreshTimed`

The value in seconds, after which the adunit will be refreshed.

Default: `0`

```javascript
{
	"refreshTimed" : "30"
}
```

#### `refreshViewable`

The value in seconds, after an adunit will be refreshed after is viewable.

Default: `0`

```javascript
{
	"refreshViewable" : "30"
}
```

#### `css`

CSS styles to apply to the div.

Default: -

```javascript
{
	"css" : "padding: 2px; marging: 1px;"
}
```

#### `bids`

The prebid bidders config for each network. Full list of prebid bidders and parameters can be found [here](https://docs.prebid.org/dev-docs/bidders.html)

```javascript
{
	"bids": [
		{"bidder": "onetag",	"params": { "pubId": "abc"}},
		{"bidder": "amx", 		"params": {"tagId": "dfg"}},
		{"bidder": "connectad", "params": {"siteId": 123,"networkId": 123}}
        ]
}
```

#### `bids` filters:

Will use the specific bid entry only if the condition is satisfied, otherwise the bid will be ignored.

**`size` filter**

Will use the specific bid if the size set in the filter will match one of the sizes request. Value taken is an arary of width and height. `[width,height]`

```javascript
{
	"bids": [
		{"bidder": "amx", 		"params": { "tagId": "tag-for-all-size"}},
		{"bidder": "amx", 		"params": { "tagId": "tag-for-728x90"}, "size":[728,90]},
		{"bidder": "amx", 		"params": { "tagId": "tag-for-300x250"}, "size":[300,250]}
        ]
}
```

**`device` filter**

Will use the specific bid if the device set in the filter will match the device detected. Value is an array of values from the devices listed in the [deviceDetection.json](./#devicedetectionjson) file. `["desktop","tablet","mobile"]`

```javascript
{
	"bids": [
		{"bidder": "amx", 		"params": { "tagId": "tag-for-all-device"}},
		{"bidder": "amx", 		"params": { "tagId": "tag-for-desktop"}, "device":["desktop"]},
		{"bidder": "amx", 		"params": { "tagId": "tag-for-mobile"}, "device":["mobile"]}
        ]
}
```

**`testParameter` filter**

Will use the specific bid only if the value of testParameter is set to true in the URL query string. It is used to make test pages. Example: If the value is set to `testBidder` , for the bid to be used, the URL where the wrapper is called must have in url `testBidder=true`, for example `https://example.com?testBidder=true`

```javascript
{
	"bids": [
		{"bidder": "amx", 		"params": { "tagId": "generic-tag"}},
		{"bidder": "amx", 		"params": { "tagId": "test-tag"}, "testParameter":"testBidder"}
        ]
}
```

### deviceDetection.json

This file defines the types of device that will going to be used in [adunits.json](./#adunitsjson)

The condition is based on the screen or window size, depending on how is configured in `config.json` at [deviceDetection](./#devicedetection)

Each entry is a device. Each device has two properties:

1. `name` : the name of the device
2. `condition` : a condition that needs to be respected for that device to be detected

Conditions with any of the < = > operators are accepted. Can use macro `{SCREENWIDTH}` for the size of window/screen

It must cover the full width of the screen possibilities from `0` to infinite.

The devices must not overlap.

Default:

```javascript
[
	{"name" : "desktop", 	"condition" : "{SCREENWIDTH} >= 1024"},
	{"name" : "tablet", 	"condition" : "{SCREENWIDTH} < 1024 && {SCREENWIDTH} >= 768"},
	{"name" : "mobile", 	"condition" : "{SCREENWIDTH} < 768"}
]
```

### modules.json

This file contains all modules used by prebid. Each network will have one module. The generic syntax is biddernameBidAdapter. There are some exceptions. Mentioning just a few:

* indexExchange module is named `ixBidAdapter`
* bidders using an alias will use the module of the parent bidder, e.g.: districtm, gourmetads will use `appnexusBidAdapter`

```javascript
[
    "ixBidAdapter",
    "aolBidAdapter",
    "gumgumBidAdapter",
    "openxBidAdapter",
    "yieldmoBidAdapter",
    "pubmaticBidAdapter",
    "33acrossBidAdapter",
    "conversantBidAdapter",
    "sharethroughBidAdapter",
    "rhythmoneBidAdapter",
    "onetagBidAdapter",
    "connectadBidAdapter"
]

```

## Custom Script Files

Custom script/modules files can be loaded and concatenated to the core code. For the file to be parsed it needs to have the name with following syntax: `custom_[filename].js`

e.g.: `custom_interstitial.js`

It will always start with `custom_` and the code needs to be written in javascript

Any valid javascript is allowed.

Can import any module.

Can use the macro `$$PREBID_GLOBAL$$`.

## Wrapper Key-values

The wrapper automatically sends key-values to GAM. The current key-values are:

#### Wrapper VeRsion

The major version of the wrapper. The newest version is `3`.

name: `wvr`

value: `3` for new wrapper, `2` for old wrapper

#### Prebid winner bidder (prebid specific)

Prebid sends the winning bidder name to GAM.

name: `hb_bidder`

value: the name of the bidder

#### Prebid winning bid value (prebid specific)

Prebid sends the winning bid value to GAM.

name: `hb_pb`

value: depends on pricing, default from `0.01` to `20.00`

#### Prebid winning format (prebid specific)

Prebid sends the format type of the winning bid to GAM

name: `hb_format`

value: possible values `banner`, `video`, `native`

#### Prebid winning source (prebid specific)

Prebid sends the source of the winning bid to GAM

name: `hb_source`

value: possible values `client`, `s2s`

#### Prebid winning sizes (prebid specific)

Prebid sends the sizes of the winning bid to GAM

name: `hb_size`

value: the value of the sizes in format `WIDTHxHEIGHT`

#### Prebid winning bid cache status

Sends the cache status of the winning bid to GAM

name: `hb_cs`

value: possible values `current`, `cached`, `storage`

#### Prebid bid density

Sends the count of the number of the bids received

name: `hb_bd`

value: the number of total bids

#### Adunit Not Hidden

Checks if the adunit is hidden through HTML/CSS

name: `anh`

value: `true` or `false` | for richmedia formats, the name of the format, e.g.: `adhesion`, `sticky`, `interstitial`, `peel`

#### Wrapper Iframe Enviroment

The wrapper checks if it is loaded within an iframe and what type of iframe

name: `wie`

value: `top` when wrapper is not in iframe; `friendly` when wrapper in friendly iframe, `cross` when wrapper in cross-domain iframe, `safe` when wrapper in safe frames

#### Wrapper Re-write global Conflict

By default the wrapper is trying to re-write the default prebid global variable, but if there is another wrapper using it, it will create a conflict. This detects if there is a conflict.

name: `wrc`

value: `nc` for No Conflict; `fr` for Forced Re-write set, but not conflict; `cr` for Conflicr Re-write detected

#### Click Confirmed Penalty

Google will apply a penalty if there are a lot of suspicious clicks. The penalty will make the users click twice on the ads. This is detecting if this penalty is active or not. It is not applied to the actual requests, since this can only be detected after the ad has been displayed, so everything recorded has a delay. Is to be used as a percentage, not an absolute number.

name: `ccp`

value: `unknown` if cannot be detected; `true` or `false`

#### Tab In Focus

The wrapper detects if the tab/window that the ad displayed in, is in focus or not

name: `tif`

value: `true` or `false`

#### Refresh Iteration Counter

The wrapper counts the number of refreshes

name: `refresh_count` (set on [config.json targeting.refreshIteration](./#targetingrefreshiteration))

value: the counter for the refresh

#### Refreshed Inventory Boolen

It tracks if the inventory is refreshed or not

name: `refrehsed` (set on [config.json targeting.refreshedInventory](./#targetingrefreshedinventory))

value: `true` or `false`

#### Refreshed Accelerated Seconds

The value represents the seconds the refresh was accelerated with. Must have [Refresh Accelerate](./#refreshaccelerate) enabled.

name: `ras`

value: From `0s` to `30`;

#### Display Advertisers History

The wrapper logs all GAM advertisers/companies IDs that displayed an ad

name: `dah`

value: All the GAM advertisers that displayed an ad

#### Last Advertiser Displayed

The wrapper logs the last GAM advertiser/company ID that displayed an ad

name: `lad`

value: The GAM ID of the last advertiser displayed

#### Last User Interaction

The time until the last user interaction. Interactions are: click, touch, scroll, visibility change, resize

name: `lui`

value: From `0s` to `59s`; From `1m` to `59m`; `1h` will be used for whatever exceeds that

#### Session Depth

The number of pageviews made in the current session

name: `sesdepth`

value: the pageview counter for the session

#### GAM Library Load Time

The time it takes for the GAM library to load. Relative to Wrapper Load Time.

name: `gpt_l`

value: From `0` To `10000` at `100` increments

#### Wrapper Load Time

The time it takes for the Wrapper to load. Relative to page navigation.

name: `wrap_l`

value: From `0` To `10000` at `100` increments

#### Page Load Time

The time it takes for the page to load. This is important for the wrapper, since the first process of the ads happens when the page is loaded.

name: `page_r`

value: From `0` To `10000` at `100` increments

#### Wrapper Analytics Auction Init

The time it takes for the auction to be initiated. Relative to wrapper load time.

name: `waai`

value: From `0` To `10000` at `100` increments

#### Wrapper Analytics Auction End

The time it takes for the auction to end. Relative the auction init.

name: `waae`

value: From `0` To `10000` at `100` increments

#### Wrapper Analytics Bidders Timeout

The bidders that timeout. Relative to prebid.

name: `wabt`

value: The name of the bidders

#### Wrapper Analytics Bidders Timeout Real

The bidders that timeout before the bids are sent to GAM.

name: `wabtr`

value: The name of the bidders

#### CoNnection DownLink

The connection downlink speed

name: `cndl`

value: 0 unknown, 0.1 under 1MB, 1 for 1-9.99 MB, 10 for 10-99.99, 100 for 100 - 999.99, 1000 for >= 1000

#### CoNnection RTT

The connection Round Trip Time speed

name: `cnrtt`

value: 0 unknown, 10 <=10, 100 <= 100,... ,900 <= 900, 1000 >= 1000

#### CoNnection TyPe

The connection type

name: `cntp`

value: na, bluetooth, cellular, ethernet, none, wifi, wimax, other, unknown

#### CoNnection Effective Type

The connection speed type

name: `cnet`

value: na, 'slow-2g', '2g', '3g', '4g', '5g'

#### CoNnection Save Data

The connection has save data enabled or not

name: `cnsd`

value: true or false

#### Page Ad Density PreRequest

Represents ad densitity before requests based on biggest ad size available

name: `padpr`

value: from `0` to `100`

#### Page Ad Density

Represents ad density after ads had been rendered

name: `pad`

value: from `0` to `100`

#### Google Ad Manager Identification

GAM sets a cookie to identify the user. Checks if that cookie exists or not.

name: `gads`

value: `true` or `false`

#### Cookies enabled

Checks if cookies for 3rd party scope are enabled by the browser

name: `cookieEnabled`

value: `true` or `false` or `na`

#### User Identifications Services/Providers

The wrapper checks what User Identification services detected the user.

name: `uids`

value: Name of the uids

#### User Identifications Services/Providers Count

The wrapper counts how many User Identification Services detected the user.

name: `uids_c`

value: Number of the UIDs services

#### Topics API interests

The interests according to Topics API [How topics are curated and selectec](https://developers.google.com/privacy-sandbox/relevance/topics#manually-curated)

name: `topicsapi`

value: Topics API Taxonomy

#### Individual User Identifications Services/Providers

It will set a key value for each User Identification Service/Provider configured and set the value `true` or `false` if the user has been identified

name: Name of the Service/Provider configured

values: `true` or `false`

#### UTMs utm\_campaign

The wrapper check is the UTMs are set in the URL and sends the value to GAM

name: `utm_campaign`

value: value of the utm\_campaign

#### UTMs utm\_source

The wrapper check is the UTMs are set in the URL and sends the value to GAM

name: `utm_source`

value: value of the utm\_source

#### UTMs utm\_medium

The wrapper check is the UTMs are set in the URL and sends the value to GAM

name: `utm_medium`

value: value of the utm\_medium

#### UTMs utm\_content

The wrapper check is the UTMs are set in the URL and sends the value to GAM

name: `utm_content`

value: value of the utm\_content

#### Device

Checks the device type

name: `device`

value: `desktop`, `tablet`, `mobile`

#### Browser

Checks the browser

name: `browser`

value: `edge`, `chrome`, `firefox`, `safari`, `opera`, `ie`, `other`

#### Operating System

Checks the operating system

name: `os`

value: `linux`, `mac`, `windows`, `other`

#### Browser Language Code

Checks the browser language code

name: `blc`

value: `sq`, `ar-SA`, `ar-EG`, `ar-DZ`, `ar-TN`, `ar-YE`, `ar-SY`, `ar-LB`, `ar-AE`, `ar-BH`, `eu`, `ca`, `zh-TW`, `zh-HK`, `zh-SG`, `cs`, `nl`, `nl-BE`, `en-US`, `en-GB`, `en-CA`, `en-IE`, `en-ZA`, `en-BZ`, `et`, `fo`, `fr`, `fr-CA`, `fr-CH`, `gd`, `de`, `de-CH`, `de-LU`, `de-LI`, `hi`, `is`, `id`, `it-CH`, `ko`, `lv`, `mk`, `no`, `pt-BR`, `rm`, `ro`, `ru`, `sz`, `sr`, `sl`, `es-AR`, `es-GT`, `es-PA`, `es-DO`, `es-VE`, `es-PE`, `es-EC`, `es-PY`, `es-SV`, `es-HN`, `es-PR`, `sv`, `sv-FI`, `ts`, `tn`, `ur`, `vi`, `xh`, `zu`, `ar-LY`, `bg`, `en-EG`, `fi`, `el`, `mt`, `sb`, `es-UY`, `tr`, `af`, `ar-IQ`, `ar-MA`, `ar-OM`, `ar-JO`, `ar-KW`, `ar-QA`, `be`, `zh-CN`, `hr`, `da`, `en`, `en-AU`, `en-NZ`, `en-JM`, `en-TT`, `fa`, `fr-BE`, `fr-LU`, `gd-IE`, `de-AT`, `he`, `hu`, `it`, `ja`, `lt`, `pl`, `pt`, `ro-MO`, `ru-MI`, `sk`, `es`, `es-CR`, `es-MX`, `es-CO`, `es-CL`, `es-BO`, `es-NI`, `sx`, `th`, `uk`, `ve`, `ji`, `unknown`

## RichMediaFormats Modules

RichMediaFormats are custom scripts that allow to manipulate the way the ads are display and interact on the page. RichMediaFormats Modules allows to create custom RichMediaFormats with any functionality.

RichMediaFormats Modules can be added as a [Custom script file](./#custom-script-files) in the src folder of the wrapper.

Here is an example of a RichMediaFormat Module: [Example](https://gitlab.com/adops/wrapper-richmediaformats/-/blob/master/example/src/custom_exampleRichMediaFormatAdapter.js). It is just adds a label overlay over the ad to display the name of the adunit.

#### Importing RichMedia Manager

This will provide the modular functionality inside the wrapper. This is required for the modularization to work.

```javascript
import richMediaManager from '../../src/_richMediaManager.js';
```

#### Add format to RichMedia Manager

Calling the function `addFormat` will add the format to the RichMedia Manager and can be used in the wrapper. The function is taking as parameter an object with the following properties:

1. `name` : The name of the RichMediaFormat. This name is used in the module and defining adunits as [RichMediaFormats](./#richmediaformat)
2. `options` : An object that holds all the default values for the options. While is not required to define a default for all the options, it is recommended that you have a default value for every option
3. `bannerPos` : The ORTB 2.0 definition of position of the ad in page

> 0 - Unknown\
> 1 - ATF\
> 2 - DEPRECATED\
> 3 - BTF

4. `initFunc` : This function is called after the ad divs are processed in page. Takes as a parameter the DOM element of ad div. Can alter or mutate ad divs before the ads are rendered
5. `registerListener` : This function is called when GAM slotRenderEnded event is fired. It takes as parameters the DOM element of the ad div and the GAM event parameter. Here you can render the ad.

```javascript
export const richMediaFormat = {
	"name" : "example",
	"options" : {"option1":"value1"},
	"bannerPos" : 0,
	"initFunc" : function(el){},
	"registerListener" : function(el,event){}
};

richMediaManager.addFormat(richMediaFormat);
```

## RichMediaFormats Core Modules

Some richmedia formats are built-in as core modules and can be included and configured directly in the wrapper.

To add a core module, add the richmedia format module to `modules.json` file.

Can configure an adunit in one of two ways:

1. In `adunits.json`, setting the property `richMediaFormat`

```json
{
   "adunit":"/22181265/Test_abcd",
   "device":["desktop","tablet","mobile"],
   "mediaTypes":{"banner":{"sizes":[[728,90]]}},
   "additionalNonHBSizes":[],
   "targeting":{"pos":"top"},
   "lazyLoaded":"false",
   "richMediaFormat":{"type":"adhesion","options":{"closeButton":"true"}},
   "refreshTimed":"0",
   "refreshViewable":"0",
   "css":"border: 0px;",
   "bids" : [
         {"bidder":"appnexus","params":{"placementId":"123456"}}
   ]
}
```

2. In body div tag, setting the data attribute `data-aa-rich-media-format`

```html
<div data-aaad='true' data-aa-adunit='/22181265/Test_abcd' data-aa-rich-media-format='{"type":"adhesion","options":{"closeButton":"true"}}'></div>
```

#### Adhesion

Type: `adhesion`

Module: `adhesionRichMediaFormatAdapter`

Options:

* `closeButton` - If set to `true` it will add a close button that will close the ad
* `position` - Possible values `header` and `footer`. It will place the adhesion at the header or footer of the page
* `bottomPosition` - The distance from the bottom of the page. It takes any CSS value for bottom property
* `topPosition` - The distance from the top of the page. It takes any CSS value for top property
* `autoClose` - Integer value representing seconds until the format will automatically close
* `closePermanent` - If is set to `true` when a user closes the format it will also destroy the adunit

#### Sticky

Type: `sticky`

Module: `stickyRichMediaFormatAdapter`

Options:

* `closeButton` - If set to `true` it will add a close button that will close the ad
* `top` - Integer in pixel representing distance from the top. It MUST be used in conjunction with `right` or `left`
* `bottom` - Integer in pixel representing distance from the bottom. It MUST be used in conjunction with `right` or `left`
* `left` - Integer in pixel representing distance from the left. It MUST be used in conjunction with `top` or `bottom`
* `right` - Integer in pixel representing distance from the right. It MUST be used in conjunction with `top` or `bottom`
* `autoClose` - Integer value representing seconds until the format will automatically close
* `closePermanent` - If is set to `true` when a user closes the format it will also destroy the adunit

#### Peel Ad

Type: `peel`

Module: `peelRichMediaFormatAdapter`

Options:

* `closeButton` - If set to `true` it will add a close button that will close the ad
* `top` - Integer in pixel representing distance from the top. It MUST be used in conjunction with `right` or `left`
* `bottom` - Integer in pixel representing distance from the bottom. It MUST be used in conjunction with `right` or `left`
* `left` - Integer in pixel representing distance from the left. It MUST be used in conjunction with `top` or `bottom`
* `right` - Integer in pixel representing distance from the right. It MUST be used in conjunction with `top` or `bottom`
* `closePermanent` - If is set to `true` when a user closes the format it will also destroy the adunit

#### Interstitial

Type: `interstitial`

Module: `interstitialRichMediaFormatAdapter`

Options:

* `timeOpen` - Integer value representing the time in seconds until the close button is available to click
* `timeOpenShow` - If `true` it will show the timer until the close button is available
* `maxDisplayPerSession` - Integer value representing the maximum displays in a session
* `pageviewDisplayStart` - Integer value represendint the first pageview count when the intersitital will display
* `pageviewDisplayEvery` - Integer value representing when to display the interstitial repeatedly based on pageview count
* `backgroundColor` - Hex value or other CSS color representing the background of the interstitial
* `opacity` - Float value representing the CSS opacity of the background

## Release Notes

#### 6.31.0 (2025-03-05)

* Update to prebid 9.31.0
* CWV data ingest abtest data

#### 6.30.0 (2025-02-25)

* Update to prebid 9.30.0
* Analytics track bidder errors
* Analytics track too many ads on page

#### 6.29.0 (2025-02-18)

* Update to prebid 9.29.0
* Bot Detection and Block initial release
* Analytics improvements

#### 6.28.0 (2025-02-11)

* Update to prebid 9.28.0
* Change cache stored bids from session to local

#### 6.27.0 (2025-02-04)

* Update to prebid 9.27.0

#### 6.26.0 (2025-01-28)

* Update to prebid 9.26.0

#### 6.25.0 (2025-01-14)

* Update to prebid 9.25.0
* Criteo networkId convert to string for S2S validation

#### 6.24.0 (2025-01-07)

* Update to prebid 9.24.0

#### 6.23.0 (2024-12-31)

* Update to prebid 9.23.0
* Fix Storage bids

#### 6.22.0 (2024-12-24)

* Limit S2S user Syncs to 20
* Console USP API deprecation notice

#### 6.21.0 (2024-12-17)

* Update to prebid 9.22.0
* cookiesEnable do not throw error when messages are not stringify JSON
* for key value mapping - the key value must be string to map, not array.
* New CMP console messages
* Fix Config page on Console

#### 6.20.0 (2024-12-10)

* Update to prebid 9.21.0
* Update bidder list that do not support safe frames

#### 6.19.0 (2024-12-03)

* Update to prebid 9.20.0
* Change how Disabled Bidders to Storage works and make it configurable
* Make Disable Bidders To Storage configurable: [Wrapper disableBiddersToStorage](./#wrapperdisablebidderstostorage)
* Analytics track sampling
* Track Pageview with no ads
* Track CWV
* Topics API A/B test key values

#### 6.18.0 (2024-11-26)

* Update to prebid 9.19.0
* Do not add bidder on bidderError if is a s2s request

#### 6.17.0 (2024-11-19)

* Update to prebid 9.18.0

#### 6.16.0 (2024-11-12)

* Update to prebid 9.17.0
* pauseOutOfView adunit level

#### 6.15.0 (2024-11-05)

* Update to prebid 9.16.0
* Fast-Forward NextMillennium PR: [12304](https://github.com/prebid/Prebid.js/pull/12304)
* Fix `hb_bd` and `hb_cs` after targeting update
* Failsafe to not observe elements if in view multiple times

#### 6.14.0 (2024-10-29)

* Added some s2s networks

#### 6.13.0 (2024-10-22)

* Remove KueezRTB from disabled when safe frame is enabled
* Added `dfp.houseFallback`
* Added `targeting.domain`
* Added destroy(null,false) second parameter to not remove the element.
* Added `targeting.id`
* Added `targeting.code`

#### 6.12.0 (2024-10-15)

* Update to prebid 9.15.0
* Added config.refresh.pauseOutOfView
* Remove elements on destroy

#### 6.11.0 (2024-10-08)

* Update to prebid 9.14.0
* Added Refreshed Accelerate Seconds Key Value Targeting
* Track Wrapper Version

#### 6.10.0 (2024-10-01)

* Update to prebid 9.13.0

#### 6.9.0 (2024-09-24)

* Update to prebid 9.12.0
* Added to s2s default bidders: "adyoulike","ix","iqzone","mediafuse","minutemedia","oms","richaudience","rise","sharethrough","unruly","rubicon"

#### 6.8.0 (2024-09-17)

* Send IP to bidders as ortb2
* Track FallbackImpressions with different events for failedToFetchFallback and emtpyCreativeFallback
* Refactor fallback renderer
* Refactor refresh queue to prepare to make refresh available only when in view

#### 6.7.1 (2024-09-11)

* Fix issue when cookie 3rd party iframe is inserted when no body is available

#### 6.7.0 (2024-09-10)

* Update to prebid 9.11.0
* Changed `targeting.cookieEnabled` to test cookies from 3rd party domains
* Added `refresh.continueOnEmpty` [more info](./#refreshcontinueonempty)

#### 6.6.0 (2024-09-03)

* Update to prebid 9.10.0
* Removed get domain helper function

#### 6.5.0 (2024-08-27)

* Update to prebid 9.9.0
* Added `targeting.cookieEnabled`
* Remove `targeting.tcf`

#### 6.4.0 (2024-08-20)

* Update to prebid 9.8.0
* Added targeting.refreshedInventory, key value to track if inventory is refreshd or not

#### 6.3.0 (2024-08-13)

* Update to prebid 9.7.0
* Fast forward PR Fix: [PR 12093](https://github.com/prebid/Prebid.js/pull/12093)

#### 6.2.0 (2024-08-06)

* Update to prebid 9.6.0
* Change consent timeout to 1000ms
* Change GPP notifications to console

#### 6.1.0 (2024-07-30)

* Update to prebid 9.5.0
* Remove s2s timeout from config

#### 6.0.0 (2024-07-23)

* Update to prebid 9.4.1
* Update GPP tracking
* Remake GeoFilter User ID
* Increase maxEntryPoints for webpack
* Update consentModule TCF
* Remove useless comments from final build
* Track Script Src URL
* Coditional Timeout
* Prebid server change config `timeout` to `maxTimeout`

#### 5.50.0 (2024-07-16)

* PB9 - Hard-code topicsAPI Iframes since they will not be available anymore: [PR 11579](https://github.com/prebid/Prebid.js/pull/11579)
* PB9 - Prebid Server timeout changed to maxTimeout: [PR 11609](https://github.com/prebid/Prebid.js/pull/11609)
* Change `timeout` to `maxTimeout` picking up values from config.json
* PR9 - Remove enrichmentFpdModule: [PR 11401](https://github.com/prebid/Prebid.js/pull/11401)
* PR9 - Require node.js 20+ [PR 11528](https://github.com/prebid/Prebid.js/pull/11528), recommanding using 20.15.1 LTS
* PR9 - Update Babel Core 7.24.6 [PR 11729](https://github.com/prebid/Prebid.js/pull/11729)
* Remove USP (US CMP API), it is deprecated [IAB USPrivacy](https://github.com/InteractiveAdvertisingBureau/USPrivacy/blob/master/CCPA/US%20Privacy%20String.md#the-us-privacy-signal-has-been-deprecated-as-of-january-31-2024-we-strongly-advise-all-users-of-the-us-privacy-string-to-transition-to-the-global-privacy-platform)
* PR9 - USP consent not sent to bidders [PR 11407](https://github.com/prebid/Prebid.js/pull/11407)

#### 5.49.0 (2024-07-09)

* Clean up code

#### 5.48.0 (2024-07-02)

* Prepare for prebid 9

#### 5.47.0 (2024-06-25)

* Upgraded to prebid 8.52.0
* Added lazyloadfetchportview to setWrapperConfig
* When safeframes are enabled, disable bidders that are not safe frame ok

#### 5.46.0 (2024-06-18)

* Upgraded to prebid 8.51.0
* Fix automatic disable of SafeFrames and S2S if custom prefix is set
* PubProvidedID module - if none are returned, return undefined to set the correct targeting
* PubProvidedID module - take eidsFunction as a string in config and execute the window function with that name

#### 5.45.0 (2024-06-11)

* Upgraded to prebid 8.50.0

#### 5.44.0 (2024-06-04)

* Upgraded to prebid 8.49.0
* Update webpack to not split into core-chunk

#### 5.43.0 (2024-05-28)

* Upgraded to prebid 8.48.0
* Update console to 0.6.3 version (fix CWV deleting navigation Types)
* Remove IDWard RTD
* Remove AdRizer Analytics

#### 5.43.0 (2024-05-21)

* Upgraded to prebid 8.47.0
* Update console to 0.6.2 version (added refresh to console, fix CWV)
* Debugging standalone fixed, revert hard-coded fix

#### 5.42.0 (2024-05-14)

* Upgraded to prebid 8.46.0
* Do not throw webpack entrypoint size errors/warnings
* Added cat3 analytics

#### 5.41.1 (2024-05-02)

* Fix Constants for targetingPrefix

#### 5.41.0 (2024-04-30)

* Upgraded to prebid 8.45.0 (skipped version 8.44.0, directly to this version due to [PR 11356](https://github.com/prebid/Prebid.js/issues/11356), which stopped Safe Frames creatives from working)

#### 5.40.0 (2024-04-23)

* Upgraded to prebid 8.43.0
* Impression tracking for fallback renderer
* Suppress Stale Render - enabled [More on stale rendering](https://docs.prebid.org/dev-docs/publisher-api-reference/setConfig.html#more-on-stale-rendering)
* If targeting prefix is set, disable safe frames
* If forceSafeFramesUnderFloor applies, disable amazon
* Temporary fix for [Issue 11365](https://github.com/prebid/Prebid.js/issues/11365)

#### 5.39.0 (2024-04-16)

* Upgraded to prebid 8.42.0
* Disable S2S when custom targeting prefix is set

#### 5.38.0 (2024-04-09)

* Upgraded to prebid 8.41.0

#### 5.37.0 (2024-04-02)

* Upgraded to prebid 8.40.0
* Ad Expansion: not set keep UI settings
* Added taboola endpoint to Topics API

#### 5.36.0 (2024-03-26)

* Upgraded to prebid 8.39.0
* Added config.dfp.adExpansion to configure [googletag.config.PageSettingsConfig\_adExpansion](https://developers.google.com/publisher-tag/reference#googletag.config.PageSettingsConfig_adExpansion)

#### 5.35.0 (2024-03-19)

* Upgraded to prebid 8.38.0

#### 5.34.0 (2024-03-12)

* Upgraded to prebid 8.37.0
* Added Topics API OpenX Endpoint
* Added Topics API Onetag Endpoint
* Update analytics endpoints

#### 5.33.0 (2024-03-05)

* Upgraded to prebid 8.36.0
* Do not build creative for local testing
* Fast-forward PR [PR 11122](https://github.com/prebid/Prebid.js/pull/11122) for conversant fix
* Enable user-agent-client-hints [enrichmentFpdModule.html#user-agent-client-hints](https://docs.prebid.org/dev-docs/modules/enrichmentFpdModule.html#user-agent-client-hints)
* Fast-forward Fix gulpfile [PR 11098](https://github.com/prebid/Prebid.js/pull/11098)

#### 5.32.0 (2024-02-27)

* Upgraded to prebid 8.35.0
* Fast-forward PR: [PR 11128](https://github.com/prebid/Prebid.js/pull/11128) for insticator fix
* Added ImproveDigital caller for TopicsAPI

#### 5.31.0 (2024-02-20)

* Upgraded to prebid 8.34.0
* Changed UTM targeting from cookies to localStorage
* Remove cookie helper functions and use storageManager functions
* Changed \_\_gads cookie and targeting to use storageManager
* Remove Liveramp ATS

#### 5.30.0 (2024-02-13)

* Upgraded to prebid 8.33.0

#### 5.29.0 (2024-02-06)

* Upgraded to prebid 8.32.0
* Added `config.dfp.forceSafeFramesForAdunits`
* Change some console logs parameters to be more informative and cleaner
* Analytics - track referrer for cross-domain iframes as urlTop

#### 5.28.0 (2024-01-30)

* Upgraded to prebid 8.31.0
* Make `topicsapi` keyvalue configurable

#### 5.27.0 (2024-01-23)

* Upgraded to prebid 8.30.0
* Send `topicsapi` GAM key value based on Topics API picked up
* Remove Save Ads functionality (to be re-built later on)
* Added URL parameter to enable overlays: `$$PREBID_GLOBAL$$Overlay`
* Remove dependency on cookies to enable console, using localStorage and without storing any user input values

#### 5.26.0 (2024-01-16)

* Upgraded to prebid 8.29.0
* Enabled Topics API Module
